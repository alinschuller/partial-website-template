<!DOCTYPE HTML>
<html>
	<head>
		<title>Online Recruitment System</title>
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		</script>
	    <script type="text/javascript" src="js/jquery.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
		<script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
		<script src="js/login.js"></script>
		<link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
		<script type="text/javascript" src="js/jquery.mmenu.js"></script>
			<script type="text/javascript">
				$(function() {
					$('nav#menu-left').mmenu();
				});
		</script>
	</head>
	<body>
			<div id="page">
					<div id="header">
						<a class="navicon" href="#menu-left"> </a>
					</div>
					<nav id="menu-left">
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li><a href="about.jsp">About</a></li>
							<li><a href="uploadcv.jsp">Upload your CV</a></li>
							<div class="clear"> </div>
						</ul>
					</nav>
			</div>
			<div class="header">
				<div class="wrap">
				<div class="header-left">
					<div class="logo">
						<a href="index.jsp">Recruitment System</a>
					</div>
				</div>
				<div class="header-right">
					<div class="top-nav">
					<ul>
						<li><a href="index.jsp">Home</a></li>
						<li><a href="about.jsp">About</a></li>
						<li><a href="uploadcv.jsp">Upload your CV</a></li>
					</ul>
				</div>
				<div class="sign-ligin-btns">
					<ul>
						<li id="loginContainer"><a class="login" id="loginButton" href="#"><span><i>Login</i></span></i></a>
							 <div class="clear"> </div>
				                <div id="loginBox">                
				                    <form id="loginForm">
				                        <fieldset id="body">
				                            <fieldset>
				                                <label for="email">Email Address</label>
				                                <input type="text" name="email" id="email" />
				                            </fieldset>
				                            <fieldset>
				                                <label for="password">Password</label>
				                                <input type="password" name="password" id="password" />
				                            </fieldset>
				                            <label class="remeber" for="checkbox"><input type="checkbox" id="checkbox" />Remember me</label>
				                            <input type="submit" id="login" value="login" />
				                        </fieldset>
				                        <span><a href="#">Forgot your password?</a></span>
				                    </form>
				                </div>
						</li>
						<div class="clear"> </div>
					</ul>
				</div>
				<div class="clear"> </div>
				</div>
				<div class="clear"> </div>
			</div>
			</div>
			<div class="text-slider">
				<div class="wrap"> 
			<div id="da-slider" class="da-slider">
					<div class="da-slide">
						<h2>Welcome to our Recruitment System</h2>
					</div>
					<div class="da-slide">
						<h2> </h2>
					</div>
					<div class="da-slide">
						<h2> </h2>
					</div>
					<nav class="da-arrows">
						<span class="da-arrows-prev"> </span>
						<span class="da-arrows-next"> </span>
					</nav>
			</div>
				<script type="text/javascript" src="js/jquery.cslider.js"></script>
				<script type="text/javascript">
					$(function() {
						$('#da-slider').cslider({
							autoplay	: true,
							bgincrement	: 450
						});
					
					});
				</script>
			 </div>
			</div>
			<div class="content">
				<div class="wrap">
					<div class="top-grids">
						<div class="top-grid">
							<div class="product-pic frist-product-pic">
								<img src="images/watch-img.png" title="google drive" />
							</div>
							<span><label>1</label></span>
							<div class="border"> </div>
							<a>Upload your Google Drive link</a>
						</div>
						<div class="top-grid">
							<div class="product-pic">
								<img src="images/shoe-img.png" title="Sit back/relax" />
							</div>
							<span><label>2</label></span>
							<div class="border hide"> </div>
							<a>Sit back and relax</a>
						</div>
						<div class="top-grid hide">
							<div class="product-pic">
								<img src="images/lap-img.png" title="results" />
							</div>
							<span><label>3</label></span>
							<a>Get Results</a>
						</div>
						<div class="clear"> </div>
					</div>
					</div>
				<div class="footer-grids">
					<div class="wrap">
						<div class="footer-grid">
							<h3>Quick Links</h3>
							<ul>
								<li><a href="#">Home</a></li>
								<li><a href="#">About</a></li>
								<li><a href="#">Upload your CV</a></li>
								<li><a href="#">Login</a></li>
							</ul>
						</div>
						<div class="footer-grid">
							<h3>Contact us</h3>
							<form>
								<input type="text" class="text" value="Your Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Your Email';}">
								<input type="text" class="text" value="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">
								<input type="submit" value="Send" />
							</form>
						</div>
						<div class="clear"> </div>
					</div>
				</div>
			</div>
	</body>
</html>

