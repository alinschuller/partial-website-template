<!DOCTYPE HTML>
<html>
	<head>
		<title>Online Recruitment System</title>
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		</script>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script src="js/login.js"></script>
		<link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
		<script type="text/javascript" src="js/jquery.mmenu.js"></script>
			<script type="text/javascript">
				$(function() {
					$('nav#menu-left').mmenu();
				});
		</script>
	</head>
	<body>
			<div id="page">
					<div id="header">
						<a class="navicon" href="#menu-left"> </a>
					</div>
					<nav id="menu-left">
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li><a href="about.jsp">About</a></li>
							<li><a href="uploadcv.jsp">Upload your CV</a></li>
							<div class="clear"> </div>
						</ul>
					</nav>
			</div>
			<div class="header">
				<div class="wrap">
				<div class="header-left">
					<div class="logo">
						<a href="index.jsp">Recruitment System</a>
					</div>
				</div>
				<div class="header-right">
					<div class="top-nav">
					<ul>
						<li><a href="index.jsp">Home</a></li>
						<li><a href="about.jsp">About</a></li>
						<li><a href="uploadcv.jsp">Upload your CV</a></li>
					</ul>
				</div>
				<div class="sign-ligin-btns">
					<ul>
						<li id="loginContainer"><a class="login" id="loginButton" href="#"><span><i>Login</i></span></i></a>
							 <div class="clear"> </div>
				                <div id="loginBox">                
				                    <form id="loginForm">
				                        <fieldset id="body">
				                            <fieldset>
				                                <label for="email">Email Address</label>
				                                <input type="text" name="email" id="email" />
				                            </fieldset>
				                            <fieldset>
				                                <label for="password">Password</label>
				                                <input type="password" name="password" id="password" />
				                            </fieldset>
				                            <label class="remeber" for="checkbox"><input type="checkbox" id="checkbox" />Remember me</label>
				                            <input type="submit" id="login" value="login" />
				                        </fieldset>
				                        <span><a href="#">Forgot your password?</a></span>
				                    </form>
				                </div>
						</li>
						<div class="clear"> </div>
					</ul>
				</div>
				<div class="clear"> </div>
				</div>
				<div class="clear"> </div>
			</div>
			</div>
			<div class="content">
					<div class="about">
						<div class="wrap">
							<h1>About <span>our Recruitment System</span></h1>
							<div class="panels">
								<div class="panel-left">
									<span> </span>
								</div>
								<div class="clear"> </div>
							</div>
						</div>
						<div class="testimonials">
							<div class="wrap">
							<div class="testimonial-head">
								<h1><span>Recruitment System</span> Stuff</h1>
								<p>Why our recruitment system is awesome</p>
							</div>
							<div class="testimonial-grids">
								<div class="testimonial-grid">
									<a href="#"><img src="images/person1.png" alt=""></a>
									<p>This is bla bla bla</p>
									<a href="#">Alin, Schüller</a>
								</div>
								<div class="testimonial-grid">
									<a href="#"><img src="images/person2.png" alt=""></a>
									<p>We have bla bla</p>
									<a href="#">Paul, Stoia</a>
								</div>
								<div class="testimonial-grid">
									<a href="#"><img src="images/person3.png" alt=""></a>
									<p>Bla bla is cool</p>
									<a href="#">Samuel, Pallo</a>
								</div>
								<div class="testimonial-grid">
									<a href="#"><img src="images/person4.png" alt=""></a>
									<p>Cool is bla bla</p>
									<a href="#">Andrei, Vararu</a>
								</div>
								<div class="clear"> </div>
							</div>
				  		</div>
				  		<div class="clients-slider">
						<!-- Dedesubt sunt imagini dinamice cool-->
						<div class="nbs-flexisel-container"><div class="nbs-flexisel-inner"><ul class="flexiselDemo3 nbs-flexisel-ul" style="left: -175.2px; display: block;">
						<li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 175.2px;"><img src="images/c3.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 175.2px;"><img src="images/c4.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 175.2px;"><img src="images/c1.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 175.2px;"><img src="images/c2.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 175.2px;"><img src="images/c3.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 175.2px;"><img src="images/c4.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 175.2px;"><img src="images/c1.png"></li><li onclick="location.href='#';" class="nbs-flexisel-item" style="width: 175.2px;"><img src="images/c2.png"></li></ul><div class="nbs-flexisel-nav-left" style="top: 13px;"></div><div class="nbs-flexisel-nav-right" style="top: 13px;"></div></div></div> 
						<div class="clear"> </div>      
							<script type="text/javascript" src="js/jquery.flexisel.js"></script>
							<script type="text/javascript">
							$(window).load(function() {
							    $(".flexiselDemo3").flexisel({
							        visibleItems: 5,
							        animationSpeed: 1000,
							        autoPlay: true,
							        autoPlaySpeed: 3000,            
							        pauseOnHover: true,
							        enableResponsiveBreakpoints: true,
							        responsiveBreakpoints: { 
							            portrait: { 
							                changePoint:480,
							                visibleItems: 1
							            }, 
							            landscape: { 
							                changePoint:640,
							                visibleItems: 2
							            },
							            tablet: { 
							                changePoint:768,
							                visibleItems: 3
							            }
							        }
							    });
							});
							</script>
						</div>
					</div>
				</div>
				<div class="footer-grids">
					<div class="wrap">
						<div class="footer-grid">
							<h3>Quick Links</h3>
							<ul>
								<li><a href="#">Home</a></li>
								<li><a href="#">About Features</a></li>
								<li><a href="#">Login</a></li>
							</ul>
						</div>
						<div class="clear"> </div>
					</div>
				</div>
			</div>
	</body>
</html>

